import { ICustomer } from "../interfaces/ICustomer";


export interface PagedCustomerList {
    pageNumber: number,
    totalLength: number,
    customerList: ICustomer[],
}
  