export interface CustomerOrderCountModel {
  custkey: string,
  name: string,
  orderCount: number
}
