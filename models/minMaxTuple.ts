export interface MinMaxTuple {
  min: number,
  max: number
}
