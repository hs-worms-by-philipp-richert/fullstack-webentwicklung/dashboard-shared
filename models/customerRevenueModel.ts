export interface CustomerRevenueModel {
  custkey: string,
  name: string,
  revenue: number|string
}
