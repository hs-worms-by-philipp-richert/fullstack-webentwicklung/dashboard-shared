export type OrderPartSuppModel = {
  partkey: bigint,
  suppkey: bigint,
  orderkey: bigint,
  quantity: number,
  supplycost: number,
  totalprice: number,
  orderdate?: Date
};
