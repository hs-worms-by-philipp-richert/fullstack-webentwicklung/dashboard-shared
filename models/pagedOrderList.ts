import { IOrder } from "../interfaces/IOrder";

export interface PagedOrderList {
  pageNumber: number,
  resultLength: number,
  totalLength: number,
  orderList: IOrder[]
}
