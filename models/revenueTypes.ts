export type LastMonthRevenue = {
  lastMonthRevenue: number
};

export type LastYearRevenue = {
  lastYearRevenue: number
};

export type AlltimeRevenue = {
  alltimeRevenue: number
};

export type ForecastNextYearRevenue = {
  forecast: number
};

export type GenericRevenue = {
  revenue: number
};
