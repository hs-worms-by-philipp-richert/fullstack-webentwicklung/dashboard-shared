import { ILineItem } from "./ILineItem";

export interface IOrder {
  orderkey: bigint,
  custkey: string,
  orderstatus: string,
  totalprice: number,
  orderdate: Date,
  orderpriority: string,
  clerk: string,
  shippriority: number,
  comment: string,
  lineitems: ILineItem[]
}
