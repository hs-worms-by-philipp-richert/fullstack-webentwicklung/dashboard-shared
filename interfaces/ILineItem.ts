export interface ILineItem {
  orderkey: bigint,
  partkey: bigint,
  suppkey: bigint,
  linenumber: bigint,
  quantity: number,
  extendedprice: number,
  discount: number,
  tax: number,
  returnflag: string,
  linestatus: string,
  shipdate: Date,
  commitdate: Date,
  receiptdate: Date,
  shipinstruct: string,
  shipmode: string,
  comment: string
}
