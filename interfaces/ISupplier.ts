export interface ISupplier {
  suppkey: bigint,
  name: string,
  address: string,
  nationkey: number,
  phone: string,
  acctbal: number,
  comment: string
}
