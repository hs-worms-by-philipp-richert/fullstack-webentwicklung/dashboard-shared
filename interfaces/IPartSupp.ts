export interface IPartSupp {
  partkey: bigint,
  suppkey: bigint,
  availqty: number,
  supplycost: number,
  comment: string
}
