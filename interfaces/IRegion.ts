export interface IRegion {
  regionkey: number,
  name: string,
  comment: string
}
