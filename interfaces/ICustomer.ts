export interface ICustomer {
  custkey: string,
  name: string,
  address: string,
  nationkey: any,
  phone: string,
  acctbal: number,
  mktsegment: string,
  comment: string
}
