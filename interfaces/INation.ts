export interface INation {
  nationkey: number,
  name: string,
  regionkey: any,
  comment: string
}
