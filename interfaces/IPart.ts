export interface IPart {
  partkey: bigint,
  name: string,
  mfgr: string,
  brand: string,
  type: string,
  size: number,
  container: string,
  retailprice: number,
  comment: string
}
